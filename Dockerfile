FROM dart:latest

# Instala las dependencias necesarias para Conduit
RUN apt-get update && \
    apt-get install -y wget gnupg && \
    wget -qO- https://dl-ssl.google.com/linux/linux_signing_key.pub | apt-key add - && \
    wget -qO- https://storage.googleapis.com/download.dartlang.org/linux/debian/dart_stable.list > /etc/apt/sources.list.d/dart_stable.list && \
    apt-get install -y postgresql-client


# Instala Conduit
RUN dart pub global activate conduit

# Establecer el directorio de trabajo y copiar el código de la aplicación
WORKDIR /app
COPY . .

# Ejecutar el comando pub get para resolver las dependencias
RUN dart pub get

# Ejecutar el comando de actualización de la base de datos
#RUN conduit db upgrade --database-config database.yaml

# Exponer el puerto 8888
EXPOSE 8888

# Ejecutar la aplicación Conduit
CMD ["conduit", "serve"]
