import 'package:api/api.dart';
import 'package:api/config/api_configuration.dart';
import 'package:api/controllers/posts_controller.dart';
import 'package:api/controllers/users_controller.dart';
import 'package:conduit_postgresql/conduit_postgresql.dart';


/// This type initializes an application.
///
/// Override methods in this class to set up routes and initialize services like
/// database connections. See http://conduit.io/docs/http/channel/.
class ApiDartChannel extends ApplicationChannel {
  ManagedContext? context;

  /// Initialize services in this method.
  ///
  /// Implement this method to initialize services, read values from [options]
  /// and any other initialization required before constructing [entryPoint].
  ///
  /// This method is invoked prior to [entryPoint] being accessed.
  @override
  Future prepare() async {
    final config = ConfigurationAPI(options!.configurationFilePath!);
    final dbConfig = config.database;
    final dataModel = ManagedDataModel.fromCurrentMirrorSystem();
    final persistentStore = PostgreSQLPersistentStore.fromConnectionInfo(
      dbConfig.username,
      dbConfig.password,
      dbConfig.host,
      dbConfig.port,
      dbConfig.databaseName,
    );

    context = ManagedContext(dataModel, persistentStore);

    CORSPolicy.defaultPolicy.allowedOrigins = [
      '*'
    ]; // Aquí deberían ir las URLs de servidores y nombres de dominios
    CORSPolicy.defaultPolicy.allowedMethods = ['GET', 'POST', 'PUT', 'DELETE'];

    logger.onRecord.listen(
      (rec) => print("$rec ${rec.error ?? ""} ${rec.stackTrace ?? ""}"),
    );
  }

  /// Construct the request channel.
  ///
  /// Return an instance of some [Controller] that will be the initial receiver
  /// of all [Request]s.
  ///
  /// This method is invoked after [prepare].
  @override
  Controller get entryPoint {
    final router = Router();

    // Prefer to use `link` instead of `linkFunction`.
    // See: https://conduit.io/docs/http/request_controller/
    router.route("/example").linkFunction((request) async {
      return Response.ok({"key": "value"});
    });

    // Ruta definida con link
    // Cada controlador permite
    /*
    2 GET
    1 POST
    1 PUT
    1 DELETE
     */
    // Obtiene todos los posts
    router.route("/posts").link(() => PostsController(context!));
    // Obtiene un post por su id
    router.route("/posts/:id").link(() => PostsController(context!));
    // Inserta un nuevo post
    router.route("/nuevo-post").link(() => PostsController(context!));
    // Actualiza un post
    router.route("/actualiza-post/:id").link(() => PostsController(context!));
    // Borra un post
    router.route("/borra-post/:id").link(() => PostsController(context!));
    // Ruta definida con link
    // Cada controlador permite
    /*
    2 GET
    1 POST
    1 PUT
    1 DELETE
     */
    //insertar usuario ruta para manejar las solicitudes POST a /nuevo-usuario
    //obtiene todos los usuarios
    router.route("/usuarios").link(() => UsersController(context!));
    // Obtiene un post por su id
    router.route("/usuarios/:id").link(() => UsersController(context!));
    // Inserta un nuevo post
    router.route("/nuevo-usuario").link(() => UsersController(context!));
    // Actualiza un post
    router.route("/actualiza-usuario/:id").link(() => UsersController(context!));
    // Borra un post
    router.route("/borra-usuario/:id").link(() => UsersController(context!));

    return router;
  }
}
