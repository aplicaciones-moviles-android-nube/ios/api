import 'package:api/api.dart';
import 'package:api/models/posts.dart'; //necesaria para las migraciones
import 'package:api/models/usuarios.dart'; //necesaria para las migraciones
import 'package:intl/intl.dart';
//ResourceController es para manejar peticiones HTTP
class PostsController extends ResourceController {
  PostsController(this.context);
  final ManagedContext context;

  @Operation.get()
  Future<Response> getAll() async {
    final query = Query<Post>(context);
    final List<Post> listaPosts = await query.fetch();

    final Response response = Response.ok(listaPosts)
      ..contentType = ContentType.json;
    return response;
  }

  @Operation.get('id')
  Future<Response> getPostById(@Bind.path('id') int id) async {
    try {
      // Crear una consulta para buscar el Post por su ID
      final query = Query<Post>(context)..where((p) => p.id).equalTo(id);

      // Ejecutar la consulta para obtener el Post
      final post = await query.fetchOne();

      if (post == null) {
        // Si no se encuentra el Post, retorna una respuesta notFound
        return Response.notFound(body: {
          "error": "El Post con el ID proporcionado no fue encontrado"
        });
      }

      // Retornar una respuesta exitosa con el Post encontrado
      return Response.ok(post)..contentType = ContentType.json;
    } catch (e) {
      // Manejar cualquier error que pueda ocurrir durante la búsqueda
      return Response.serverError(body: {"error": e.toString()});
    }
  }

  @Operation.post()
  Future<Response> crearPost(
      @Bind.body() Map<String, dynamic> requestBody) async {
    try {
      // Extraer los datos del cuerpo de la solicitud
      final String titulo = requestBody['titulo'];
      final String contenido = requestBody['contenido'];
      final String url = requestBody['url'];


      //final DateTime fechaPublicacion =DateTime.parse(requestBody['fechaPublicacion']);
      // Formatear la fecha en el formato esperado por la base de datos
    final String fechaPublicacionString = requestBody['fechaPublicacion'];
    final DateFormat formatter = DateFormat('yyyy-MM-dd HH:mm:ss.SSS');
    final DateTime fechaPublicacion = formatter.parse(fechaPublicacionString);


      final int autorId = requestBody['autor'];

      // Buscar el autor en la base de datos usando su ID
      final autor = await obtenerUsuarioPorId(autorId);
      if (autor == null) {
        return Response.notFound(body: {
          "error": "El autor con el ID proporcionado no fue encontrado"
        });
      }

      // Crear un nuevo objeto Post con los datos proporcionados
      final nuevoPost = Post()
        //..id = null // El ID se generará automáticamente
        ..titulo = titulo
        ..contenido = contenido
        ..url = url
        ..fechaPublicacion = fechaPublicacion
        ..autor = autor;

      // Insertar el nuevo post en la base de datos
      final query = Query<Post>(context)..values = nuevoPost;
      final postInsertado = await query.insert();

      // Retornar una respuesta exitosa con el post insertado
      return Response.ok(postInsertado)..contentType = ContentType.json;
    } catch (e, stackTrace) {
      // Devolver una respuesta de error con detalles adicionales
      return Response.badRequest(body: {
        "error": "Error al crear el post: ${e.toString()}",
        "stackTrace": stackTrace.toString()
      });
    }
  }

  // Función para obtener un usuario por su ID
  Future<Usuario?> obtenerUsuarioPorId(int id) async {
    final query = Query<Usuario>(context)..where((u) => u.id).equalTo(id);
    return await query.fetchOne();
  }

  @Operation.put('id')
  Future<Response> updatePost(@Bind.path('id') int id,
      @Bind.body() Map<String, dynamic> requestBody) async {
    try {
      // Extraer los datos del cuerpo de la solicitud
      final String titulo = requestBody['titulo'];
      final String contenido = requestBody['contenido'];
      final String url = requestBody['url'];
      final DateTime fechaPublicacion =
          DateTime.parse(requestBody['fechaPublicacion']);
      final int autorId = requestBody['autor'];

      // Verificar si el post con el ID proporcionado existe en la base de datos
      final postExistente = await obtenerPostPorId(id);
      if (postExistente == null) {
        return Response.notFound(body: {
          "error": "El post con el ID proporcionado no fue encontrado"
        });
      }

      // Buscar el autor en la base de datos usando su ID
      final autor = await obtenerUsuarioPorId(autorId);
      if (autor == null) {
        return Response.notFound(body: {
          "error": "El autor con el ID proporcionado no fue encontrado"
        });
      }

      // Actualizar los campos del post existente con los nuevos valores
      postExistente.titulo = titulo;
      postExistente.contenido = contenido;
      postExistente.url = url;
      postExistente.fechaPublicacion = fechaPublicacion;
      postExistente.autor = autor; // Asignar el nuevo autor

      // Actualizar el post en la base de datos
      final query = Query<Post>(context)
        ..values = postExistente
        ..where((post) => post.id).equalTo(id); // Agregar la cláusula WHERE
      final postActualizado = await query.updateOne();

      // Retornar una respuesta exitosa con el post actualizado
      return Response.ok(postActualizado)..contentType = ContentType.json;
    } catch (e) {
      // Manejar cualquier error que pueda ocurrir durante la actualización
      return Response.badRequest(body: {"error": e.toString()});
    }
  }

  @Operation.delete('id')
  Future<Response> deletePost(@Bind.path('id') int id) async {
    try {
      // Verificar si el post con el ID proporcionado existe en la base de datos
      final postExistente = await obtenerPostPorId(id);
      if (postExistente == null) {
        return Response.notFound(body: {
          "error": "El post con el ID proporcionado no fue encontrado"
        });
      }

      // Eliminar el post de la base de datos
      final query = Query<Post>(context)..where((post) => post.id).equalTo(id);
      await query.delete();

      // Retornar una respuesta exitosa
      return Response.noContent();
    } catch (e) {
      // Manejar cualquier error que pueda ocurrir durante la eliminación
      return Response.serverError(
          body: {"error": "Error al eliminar el post: ${e.toString()}"});
    }
  }

  Future<Post?> obtenerPostPorId(int id) async {
    final query = Query<Post>(context)..where((post) => post.id).equalTo(id);
    return await query.fetchOne();
  }
}
