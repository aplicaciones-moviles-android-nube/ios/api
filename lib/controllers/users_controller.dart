import 'package:api/api.dart';
import 'package:api/models/posts.dart'; //necesaria para las migraciones
import 'package:api/models/usuarios.dart'; //necesaria para las migraciones

class UsersController extends ResourceController {
  UsersController(this.context);
  final ManagedContext context;

  @Operation.get()
  Future<Response> getAll() async {
    final query = Query<Usuario>(context);
    final List<Usuario> listaUsuarios = await query.fetch();

    final Response response = Response.ok(listaUsuarios)
      ..contentType = ContentType.json;
    return response;
  }

  @Operation.get('id')
  Future<Response> getUserById(@Bind.path('id') int id) async {
    try {
      final query = Query<Usuario>(context)..where((u) => u.id).equalTo(id);
      final usuario = await query.fetchOne();

      if (usuario == null) {
        return Response.notFound(body: {
          "error": "El usuario con el ID proporcionado no fue encontrado"
        });
      }

      return Response.ok(usuario)..contentType = ContentType.json;
    } catch (e) {
      return Response.serverError(body: {"error": e.toString()});
    }
  }

 @Operation.post()
Future<Response> createUser(@Bind.body() Map<String, dynamic> requestBody) async {
  try {
    final String proveedor = requestBody['proveedor'];
    final int proveedorId = requestBody['proveedorId']; // Convertir a entero
    final String nombre = requestBody['nombre'];
    final String correoElectronico = requestBody['correoElectronico']; // Corrige el nombre del campo

    final nuevoUsuario = Usuario()
      ..proveedor = proveedor
      ..proveedorId = proveedorId
      ..nombre = nombre
      ..correoElectronico = correoElectronico;

    final query = Query<Usuario>(context)..values = nuevoUsuario;
    final usuarioInsertado = await query.insert();

    return Response.ok(usuarioInsertado)..contentType = ContentType.json;
  } catch (e) {
    return Response.badRequest(body: {"error": e.toString()});
  }
}



  @Operation.put('id')
Future<Response> updateUser(@Bind.path('id') int id, @Bind.body() Map<String, dynamic> requestBody) async {
  try {
    final String proveedor = requestBody['proveedor'];
    final int proveedorId = requestBody['proveedorId'];
    final String nombre = requestBody['nombre'];
    final String correoElectronico = requestBody['correoElectronico'];

    final usuarioExistente = await fetchUsuarioById(id);
    if (usuarioExistente == null) {
      return Response.notFound(body: {
        "error": "El usuario con el ID proporcionado no fue encontrado"
      });
    }

    // Crear un nuevo objeto Usuario con los datos proporcionados
    final nuevoUsuario = Usuario()
      ..proveedor = proveedor
      ..proveedorId = proveedorId
      ..nombre = nombre
      ..correoElectronico = correoElectronico;

    // Actualizar el usuario en la base de datos
    final query = Query<Usuario>(context)
      ..values = nuevoUsuario
      ..where((usuario) => usuario.id).equalTo(id);

    final usuarioActualizado = await query.updateOne();

    // Retornar una respuesta exitosa con el usuario actualizado
    return Response.ok(usuarioActualizado)..contentType = ContentType.json;
  } catch (e) {
    return Response.badRequest(body: {"error": e.toString()});
  }
}


  @Operation.delete('id')
  Future<Response> deleteUser(@Bind.path('id') int id) async {
    try {
      final usuarioExistente = await fetchUsuarioById(id);
      if (usuarioExistente == null) {
        return Response.notFound(body: {
          "error": "El usuario con el ID proporcionado no fue encontrado"
        });
      }

      final query = Query<Usuario>(context)..where((usuario) => usuario.id).equalTo(id);
      await query.delete();

      return Response.noContent();
    } catch (e) {
      return Response.serverError(body: {"error": "Error al eliminar el usuario: ${e.toString()}"});
    }
  }

  Future<Usuario?> fetchUsuarioById(int id) async {
    final query = Query<Usuario>(context)..where((usuario) => usuario.id).equalTo(id);
    return await query.fetchOne();
  }
}