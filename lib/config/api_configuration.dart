import 'package:api/api.dart';

class ConfigurationAPI extends Configuration {
  ConfigurationAPI(String fileName) : super.fromFile(File(fileName));
  late DatabaseConfiguration database;
}
