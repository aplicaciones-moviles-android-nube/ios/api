import 'package:api/api.dart';
import 'usuarios.dart'; 

// Clase para representar solo la fecha
class Date {
  // Constructor
  Date(this.year, this.month, this.day);
   // Método para crear una instancia de Date desde un DateTime
  factory Date.fromDateTime(DateTime dateTime) {
    return Date(dateTime.year, dateTime.month, dateTime.day);
  }

  // Propiedades
  int year;
  int month;
  int day;

 

  // Método para convertir Date a DateTime
  DateTime toDateTime() {
    return DateTime(year, month, day);
  }
}

@Table(name: 'posts')
class _Post {
  @Column(primaryKey: true, autoincrement: true)
  int? id;

  @Column()
  String? titulo;

  @Column()
  String? contenido;
  
  @Column()
  String? url;

  @Column()
  DateTime? fechaPublicacion;

  @Relate(#posts)
  Usuario? autor;
}

class Post extends ManagedObject<_Post> implements _Post {
  // Mantén la propiedad fechaPublicacion como DateTime
  // pero agrega un getter para obtener solo la fecha
  Date get fechaPublicacionDate => Date.fromDateTime(fechaPublicacion ?? DateTime.now());
}