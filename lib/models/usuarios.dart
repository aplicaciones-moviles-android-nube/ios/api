import 'package:api/api.dart';
import 'posts.dart'; // Asumiendo que importas correctamente el modelo de Post

@Table(name:'usuarios')
class _Usuario {
  @Column(primaryKey: true, autoincrement: true)
  int? id;

  @Column(unique: true, indexed: true)
  String? proveedor;

  @Column()
  int? proveedorId;

  @Column()
  String? nombre;

  @Column()
  String? correoElectronico;

  ManagedSet<Post>? posts; // La relación en este lado es una colección de posts
}

class Usuario extends ManagedObject<_Usuario> implements _Usuario {}
